### How do I get set up? ###

If you don't have Bower and NodeJS installed in your system - install it! ;)

To do that (hereinafter described commands for Ubuntu-based systems):

```
#!bash

sudo apt-get install bower
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
```
 
OR (for v.5)

```
#!bash

curl -sL https://deb.nodesource.com/setup_5.x | sudo -E bash -
sudo apt-get install -y nodejs
```

To install these packages on other OS - visit these pages:

[http://bower.io/#install-bower](Link URL)

[https://nodejs.org/en/download/](Link URL)

When everything is installed and the system is ready, we can begin to build our site.
To do that:

```
#!bash

sudo npm i gulp -g
cd path/to/our/project
bower i
npm i
gulp
gulp watch
```

Once you enter all these commands Chrome opens on our page.